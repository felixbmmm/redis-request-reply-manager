import { EventEmitter } from 'events';
import Redis, { RedisOptions } from 'ioredis';
import makeRetry from 'p-retry';
import { Logger } from './Logger';
import { LogLevel } from './LogLevel';

interface RedisReplyOptions extends EventEmitter {
  /**
   * Set redis connection string manually
   * or optionally use `process.env.REDIS_URL`.
   *
   * @example
   * const redisUrl = 'redis://user:secret@localhost:6379/0?foo=bar&qux=baz'
   */
  url?: string;
  /**
   * Level of log.
   *
   * List of levels: `info`, `debug`, `warn`, `error`.
   *
   * @default error
   */
  logLevel?: LogLevel;
  /**
   * Set up connection retry strategy.
   * Defaults to exponential backoff.
   * @see https://en.wikipedia.org/wiki/Exponential_backoff
   * @example
   * const connectionRetryStrategy = (times) => Math.min(Math.pow(2, times), 9000000)
   */
  connectionRetryStrategy?: (times: number) => number;
}

export class RedisReply extends EventEmitter {
  private readonly url: string;
  private readonly connectionRetryStrategy: (times: number) => number;
  private readonly logger: Logger;
  private readonly redisConn: Redis.Redis;

  constructor(opts?: RedisReplyOptions) {
    super();
    this.url = opts?.url || process.env.REDIS_URL || 'redis://localhost:6379';
    this.connectionRetryStrategy =
      opts?.connectionRetryStrategy ||
      function (times: number): number {
        return Math.min(Math.pow(2, times), 9000000);
      };
    const redisOptions: RedisOptions = {
      retryStrategy: this.connectionRetryStrategy,
      maxRetriesPerRequest: 0,
      showFriendlyErrorStack: true,
    };
    this.redisConn = new Redis(this.url, redisOptions);
    this.redisConn.on('error', this.logError);
    this.logger = new Logger(opts?.logLevel || LogLevel.ERROR);
  }

  /**
   * Start hearing request
   *
   * @param topic Topic for the request list
   * @param cb Callback to receive any request, consists of payload and respond
   * respond is used to send the reply's payload
   */
  start(
    topic: string,
    cb: (payload: any, respond: (payload: any) => Promise<void> | void) => Promise<void> | void,
  ): void {
    const retryOptions = {
      forever: true,
      maxTimeout: 2 * 60 * 60 * 1000 /* 2 hours */,
      onFailedAttempt: () => {
        if (this.redisConn.status === 'end') {
          /**
           * Throw error will force retry operation to abort.
           */
          throw new makeRetry.AbortError('Aborting retry due to redis disconnected');
        }
      },
    };

    makeRetry(() => this.getListContent(topic, cb), retryOptions).catch((err) => {
      if (err instanceof makeRetry.AbortError) {
        this.logInfo(topic, 'redis-publish', err.message);
      }
    });
  }

  cancel() {
    this.redisConn.disconnect();
  }

  private async getListContent(
    topic: string,
    cb: (payload: any, respond: (payloadSent: any) => Promise<void> | void) => Promise<void> | void,
  ) {
    for (;;) {
      try {
        const blpopResult: any = await this.redisConn.blpop(topic, 1);
        if (blpopResult !== null) {
          const request: any = JSON.parse(blpopResult[1]);
          const replyTo: any = request.replyTo;
          const payload: any = request.payload;
          const respondReply: any = (payloadSent: any) => {
            this.publishToReplyTo(replyTo, JSON.stringify(payloadSent));
          };
          await cb(payload, respondReply);
        }
      } catch (err: any) {
        this.logError(topic, 'list-pop', err);
      }
    }
  }

  private publishToReplyTo(replyTo: string, payload: any) {
    try {
      this.redisConn.publish(replyTo, payload);
    } catch (err: any) {
      this.logError(replyTo, 'pub-reply-to', err);
    }
  }

  private logInfo(topic: string, context: string, message: string): void {
    this.logger.info(context, topic, message, {
      topic,
    });
  }

  private logError(replyTo: string, context: string, err: Error): void {
    this.logger.error(context, replyTo, err.message, {
      replyTo,
    });
    this.emit('error', err);
  }
}

process.on('unhandledRejection', (err, p) => {
  console.error(p, err);
});
