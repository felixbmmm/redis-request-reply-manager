import { EventEmitter } from 'events';
import Redis, { RedisOptions } from 'ioredis';
import makeRetry from 'p-retry';
import { Logger } from './Logger';
import { LogLevel } from './LogLevel';

interface RedisRequestOptions extends EventEmitter {
  /**
   * Set redis connection string manually
   * or optionally use `process.env.REDIS_URL`.
   *
   * @example
   * const redisUrl = 'redis://user:secret@localhost:6379/0?foo=bar&qux=baz'
   */
  url?: string;
  /**
   * Level of log.
   *
   * List of levels: `info`, `debug`, `warn`, `error`.
   *
   * @default error
   */
  logLevel?: LogLevel;
  /**
   * Set up connection retry strategy.
   * Defaults to exponential backoff.
   * @see https://en.wikipedia.org/wiki/Exponential_backoff
   * @example
   * const connectionRetryStrategy = (times) => Math.min(Math.pow(2, times), 9000000)
   */
  connectionRetryStrategy?: (times: number) => number;
}

export class RedisRequest extends EventEmitter {
  private readonly url: string;
  private readonly connectionRetryStrategy: (times: number) => number;
  private readonly logger: Logger;
  private readonly redisConn: Redis.Redis;

  constructor(opts?: RedisRequestOptions) {
    super();
    this.url = opts?.url || process.env.REDIS_URL || 'redis://localhost:6379';
    this.connectionRetryStrategy =
      opts?.connectionRetryStrategy ||
      function (times: number): number {
        return Math.min(Math.pow(2, times), 9000000);
      };
    const redisOptions: RedisOptions = {
      retryStrategy: this.connectionRetryStrategy,
      maxRetriesPerRequest: 0,
      showFriendlyErrorStack: true,
    };
    this.redisConn = new Redis(this.url, redisOptions);
    this.redisConn.on('error', this.logError);
    this.logger = new Logger(opts?.logLevel || LogLevel.ERROR);
  }

  /**
   * Create a request
   *
   * @param topic Topic for the request list
   * @param replyTo A reply-to topic for responders to use for replies/responds
   * @param payload Payload to be sent
   * @param options Options needed for request (ex. timeout)
   * @param cb Callback to receive any response, consists of error and response
   */
  createRequest(
    topic: string,
    replyTo: string,
    payload: any,
    cb: (error: any, response: any) => Promise<void> | void,
  ): void {
    const retryOptions = {
      forever: true,
      maxTimeout: 2 * 60 * 60 * 1000 /* 2 hours */,
      onFailedAttempt: () => {
        if (this.redisConn.status === 'end') {
          /**
           * Throw error will force retry operation to abort.
           */
          throw new makeRetry.AbortError('Aborting retry due to redis disconnected');
        }
      },
    };

    this.pushToList(topic, replyTo, payload);

    /**
     * Higher order callback to execute the inner function first.
     */
    makeRetry(() => this.subscribeToReplyTo(replyTo, cb), retryOptions).catch((err) => {
      if (err instanceof makeRetry.AbortError) {
        this.logInfo(topic, 'redis-subscribe', err.message);
      }
    });
  }

  private pushToList(topic: string, replyTo: string, payload: any) {
    try {
      const constructedPayload: any = {
        replyTo,
        payload,
      };
      this.redisConn.rpush(topic, JSON.stringify(constructedPayload));
    } catch (err: any) {
      this.logError(topic, 'list-push', err);
    }
  }

  private subscribeToReplyTo(
    replyTo: string,
    cb: (error: any, response: any) => Promise<void> | void,
  ) {
    try {
      this.redisConn.subscribe(replyTo, (err, count) => {
        cb(err, null);
      });

      this.redisConn.on('message', async (channel, message) => {
        cb(null, message);
      });
    } catch (err: any) {
      this.logError(replyTo, 'sub-reply-to', err);
    }
  }

  private logInfo(topic: string, context: string, message: string): void {
    this.logger.info(context, topic, message, {
      topic,
    });
  }

  private logError(replyTo: string, context: string, err: Error): void {
    this.logger.error(context, replyTo, err.message, {
      replyTo,
    });
    this.emit('error', err);
  }
}

process.on('unhandledRejection', (err, p) => {
  console.error(p, err);
});
