import winston, { Logger as WinstonLogger } from 'winston';
import { LogLevel } from './LogLevel';

export class Logger {
  private readonly logger!: WinstonLogger;

  constructor(logLevel = LogLevel.ERROR) {
    this.logger = winston.createLogger({
      level: logLevel,
      format: winston.format.combine(
        winston.format((info) => {
          info.level = info.level.toUpperCase();
          return info;
        })(),
        winston.format.timestamp(),
        winston.format.json(),
      ),
      transports: [new winston.transports.Console({ handleExceptions: true })],
      defaultMeta: { processId: process.pid },
      exitOnError: false,
    });
  }

  info(context: string, namespace: string, message: string, meta?: Record<string, any>): void {
    this.logger.info(message, { context, namespace, ...meta });
  }

  debug(context: string, namespace: string, message: string, meta?: Record<string, any>): void {
    this.logger.debug(message, { context, namespace, ...meta });
  }

  warn(context: string, namespace: string, message: string, meta?: Record<string, any>): void {
    this.logger.warn(message, { context, namespace, ...meta });
  }

  error(context: string, namespace: string, message: string, meta?: Record<string, any>): void {
    this.logger.error(message, { context, namespace, ...meta });
  }
}
