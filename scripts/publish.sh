#!/bin/bash

set -e

if [ -n "${CI_DEPLOY_TOKEN}" ]; then
    echo -e "\n//gitlab.com/api/v4/projects/32517745/packages/npm/:_authToken=${CI_DEPLOY_TOKEN}" >>.npmrc
else
    echo "CI_DEPLOY_TOKEN env is not set." >&2
    exit 1
fi

npm publish
