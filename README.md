# Redis Request Reply Manager

This package is used to implement a Request and Reply concept using Redis as it's system.

## Installation

Before using the package, make sure to:

1. Create a `.npmrc` file inside your project's folder.
2. Fill the file with this content:

```string
@felixbmmm:registry=https://gitlab.com/api/v4/projects/32517745/packages/npm/
//gitlab.com/api/v4/projects/32517745/packages/npm/:_authToken=
```

Then, install it via npm:

```command
npm i @felixbmmm/redis-request-reply-manager
```

or yarn:

```command
yarn add @felixbmmm/redis-request-reply-manager
```

## How it works

### Request

When a Request is created, the package will create a list named `<topic_name>`, then the Request's payload will be sent there with `<reply_to>`. This `<reply_to>` is used for Reply to send the Reply's payload which will be explained shortly. After the request is sent to the list, the package will also Subscribe to a Redis' Pub/Sub channel named `<reply_to>` to receive any replies. After the request is received, a Reply will be sent to the same channel too.

### Reply

When a Reply is created, the package will get all incoming request based on the named list from `<topic_name>`. Then, each of the Requests will be processed through the callback. After the request is processed and the Reply's payload is constructed from the project side, the payload will be send through `respond` method. This method will Publish the payload to the channel named `<reply_to>`. Then, if the Reply's payload is successfully sent, the Request side will be able to receive it via `respond` parameter from the Request's callback parameter.

## Example

### Request Code Example

```typescript
const redisRequest: RedisRequest = new RedisRequest() // create a Request object
redisRequest.createRequest(
  'testing',                       // topic name. This will be used for the list's name to send the request.
  'get-guardians',                 // reply to name. This will be used to the Pub/Sub's name.
  { test: 123 },                   // request's payload. This is usually used to add information about what will the request receiver will do and what to reply.
  (error, response) => {           // callback, consists of error and response. Response will be renewed every time Reply object send a payload
    if (err) throw err
    if (res !== null && res !== '') {
      const reply = JSON.parse(res)
      console.log(reply)
    }
  }
)
```

### Reply Code Example

```typescript
const redisReply: RedisReply = new RedisReply()
redisReply.start(
  'testing',                       // topic name. This will be used for the list's name to receive requests.
  (payload, respond) => {          // callback, consists of payload and respond method. Payload is equivalent to the Request's payload, while respond method is used to send replies. Respond method must receive one parameter as the Reply's payload.
    console.log(payload)
    if (payload.test === 123) {
      respond({ message: 'It works!' })
    }
  }
)
```
